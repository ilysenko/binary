# coding: utf-8
import StringIO
import unittest
import mock

import __builtin__

VALID_XML = """<?xml version="1.0"?>
<catalog>
   <book id="bk101">
      <author>Gambardella, Matthew</author>
   </book>
</catalog>
"""

VALID_JSON = """[
  {
    "guid": "e0b2c994-597c-4087-ab2c-cf316c8f2d93",
    "index": 0
  }
]
"""

VALID_CSV = """SRM_SaaS_ES,MXASSETInterface,AddChange,EN\n
ASSETNUM,AS_SITEID,mesa01,SDASITE
"""

INVALID_FILE = "test </test"

from django.core.files.storage import Storage
from converter import file_utils


class MockStorage(Storage):
    def save(self, name, content):
        return name

    def read(self):
        return


@mock.patch('converter.file_utils.default_storage', new_callable=MockStorage)
class FileUtilsTestCase(unittest.TestCase):

    def get_file(self, data, name):
        out = StringIO.StringIO(data)
        out.__enter__ = mock.Mock(return_value=out)
        out.__exit__ = mock.Mock()
        out.name = name
        __builtin__.open = mock.Mock(return_value=out)
        return out

    @mock.patch('converter.file_utils.uuid.uuid4')
    def test_get_guid(self, uuid_mock, storage_mock):
        uuid_mock.return_value = 1234
        self.assertEqual('1234', file_utils.get_guid())

    def test_check_and_save_file(self, storage_mock):
        guid = file_utils.get_guid()

        with mock.patch('converter.file_utils.get_guid') as generator_mock:
            generator_mock.return_value = guid

            # test valid csv
            file = self.get_file(VALID_CSV, 'test.csv')
            self.assertEqual(
                '{}_{}'.format(guid, file.name),
                file_utils.check_and_save_file(file)
            )

            # test valid xml
            file = self.get_file(VALID_XML, 'test.xml')
            self.assertEqual(
                '{}_{}'.format(guid, file.name),
                file_utils.check_and_save_file(file)
            )

            # test valid json
            file = self.get_file(VALID_JSON, 'test.json')
            self.assertEqual(
                '{}_{}'.format(guid, file.name),
                file_utils.check_and_save_file(file)
            )

            # test invalid extension
            file = self.get_file(VALID_JSON, 'test.doc')
            with self.assertRaises(Exception):
                file_utils.check_and_save_file(file)

            # test invalid file data
            file = self.get_file(INVALID_FILE, 'test.xls')
            with self.assertRaises(Exception):
                file_utils.check_and_save_file(file)




