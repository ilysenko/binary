# coding: utf-8
import os
import json
import csv
import xmltodict

from converter import settings
from django.conf import settings as base_settings
from  django.core.urlresolvers import reverse


class JsonProcessor:
    @staticmethod
    def load(filename):
        with open(filename) as file:
            return json.load(file)

    @staticmethod
    def save(data, filename):
        try:
            with open(filename, 'w') as file:
                json.dump(data, file)
        except Exception:
            try:
                os.remove(filename)
            finally:
                raise Exception(u'File `{}` can not be converted to JSON'
                                .format(os.path.basename(filename)))
        return filename


class CsvProcessor:
    @staticmethod
    def load(filename):
        data = []
        with open(filename, 'r') as csvfile:
            file_dialect = csv.Sniffer().sniff(csvfile.read(1024))
            csvfile.seek(0)
            dict_reader = csv.DictReader(csvfile, dialect=file_dialect)
            for row in dict_reader:
                data.append(row)
        return data

    @staticmethod
    def save(data, filename):
        if isinstance(data, dict):
            data = [data]

        try:
            with open(filename, 'wb') as outfile:
                dict_writer = csv.DictWriter(outfile, data[0].keys())
                dict_writer.writeheader()
                dict_writer.writerows(data)
        except Exception:
            try:
                os.remove(filename)
            finally:
                raise Exception(u'File `{}` can not be con in csv'
                                .format(os.path.basename(filename)))
        return filename


class XmlProcessor:
    @staticmethod
    def load(filename):
        with open(filename) as file:
            return xmltodict.parse(file)

    @staticmethod
    def save(data, filename):
        if isinstance(data, list):
            data = {'items': {str(k): v for k, v in enumerate(data)}}
        try:
            with open(filename, 'w') as outfile:
                xmltodict.unparse(data, outfile, pretty=True)
        except Exception:
            try:
                os.remove(filename)
            finally:
                raise Exception(u'File `{}` can not be converted to XML'
                                .format(os.path.basename(filename)))
        return filename


API = {
    'JSON': JsonProcessor,
    'CSV': CsvProcessor,
    'XML': XmlProcessor
}


def get_api_by_filepath(filename):
    """
    returns api class, depending of file extension
    """
    return API.get(filename.split(".")[-1].upper())


def get_processed_files():
    """
    returns list of uploaded files
    """
    paths = []
    for i in os.listdir(base_settings.MEDIA_ROOT):
        if len(i.split('_'))>1:
            guid, name = i.split('_',2)
            #TODO: test what guid is valid
            paths.append((i, name))
    return paths


def get_constants():
    """
    constants pass to frontend
    """
    return json.dumps({
        'valid_extensions': API.keys(),
        'max_upload_size': getattr(settings, 'MAX_FILE_SIZE', 1024 * 1024 * 10),
        'api_upload_file': reverse('upload_file'),
        'get_file_url': reverse('get_file_url'),
        'delete_url': reverse('delete_all'),
        'processed_files' : get_processed_files()

    })