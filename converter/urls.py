# coding: utf-8
from django.conf.urls import patterns, url
from converter.views import DownloadFileView, UploadFileView, DeleteAllView


urlpatterns = patterns(
    '',
    url(r'^upload/$', DownloadFileView.as_view(), name='upload_file'),
    url(r'^download/$', UploadFileView.as_view(), name='get_file_url'),
    url(r'^delete_all/$', DeleteAllView.as_view(), name='delete_all'),
)