# coding: utf-8
import json
from django.http import HttpResponse
from django.views.generic import View

from converter.file_utils import (
    check_and_save_file, prepare_file_url, clear_media_folder
)


class JsonResponseMixing:
    """
    mixing for json response with status code
    """

    def json_response(self, data, code=200):
        return HttpResponse(json.dumps(data), status=code)


class DownloadFileView(View, JsonResponseMixing):
    """
    trying to parse file. Saves if file is good for selected format
    """

    def post(self, request):
        file = request.FILES['file']

        try:
            guid = check_and_save_file(file)
        except Exception as e:
            return HttpResponse(str(e), status=400)
        else:
            return self.json_response({'result': guid, 'filename': file.name})


class UploadFileView(View, JsonResponseMixing):
    """
    returns url for requested file
    """

    def get(self, request, **kwargs):
        try:
            url = prepare_file_url(request.GET.get('original_name'),
                                   request.GET.get('extension'))
        except Exception as e:
            return HttpResponse(str(e), status=400)
        else:
            return self.json_response({'url': url})


class DeleteAllView(View, JsonResponseMixing):
    """
    deleting all uploaded files (clearing media folder)
    """

    def get(self, request):
        try:
            clear_media_folder()
        except:
            return HttpResponse("Deleting files error", status=400)
        else:
            return self.json_response({'status': True})
