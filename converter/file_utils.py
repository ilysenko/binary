# coding: utf-8
import os
import shutil
import uuid

from django.conf import settings
from django.core.files.storage import default_storage

from converter.api import get_api_by_filepath, API


def get_guid():
    """
    guid generation as string
    """
    return str(uuid.uuid4())


def get_file_full_path(filename):
    return os.path.join(settings.MEDIA_ROOT, filename)


def check_and_save_file(file):
    """
    saves uploaded temp file with right filename.
    trying to parse it.
    deleting if file is invalid
    """
    guid = get_guid()
    path, name = os.path.split(file.name)
    base_name = ('.').join(name.split('.')[:-1])
    ext = name.split('.')[-1]
    dir_path = guid + '_' + file.name

    # check files extension on backend
    if not ext.upper() in API:
        raise Exception("File Extension Not Valid: {}".format(file.name))

    filename = default_storage.save(
        os.path.join(dir_path, base_name + '.' + ext.lower()),
        file
    )
    try:
        get_api_by_filepath(filename).load(get_file_full_path(filename))
    except:
        try:
            shutil.rmtree(get_file_full_path(dir_path))
        finally:
            raise Exception("File Parse Error: {}".format(file.name))
    else:
        return dir_path


def prepare_file_url(original_name, extension):
    """
    original_name - name of first uploaded file
    extension - needed format conversion
    """
    path, name = os.path.split(original_name)
    # removing extension from filename
    name = ('.').join(name.split('.')[:-1])
    requested_path = os.path.join(path, name + '.' + extension.lower())
    if not os.path.isfile(get_file_full_path(requested_path)):
        try:
            data = get_api_by_filepath(original_name)\
                .load(get_file_full_path(original_name))
        except:
            raise Exception('Error loading original file')
        get_api_by_filepath(requested_path).save(
            data, get_file_full_path(requested_path)
        )
        return os.path.join(settings.MEDIA_URL, requested_path)
    else:
        return os.path.join(settings.MEDIA_URL, requested_path)


def clear_media_folder():
    """
    deleting all uploaded files (clearing media folder)
    """
    contents = [os.path.join(settings.MEDIA_ROOT, i)
                for i in os.listdir(settings.MEDIA_ROOT)
    ]
    [shutil.rmtree(i) for i in contents if os.path.isdir(i)]





