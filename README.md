# BinaryStudio Demo by Garry Lysenko #

Application for batch converting files between JSON, XML and CSV formats. The application saves the state between sessions.

# Live Demo http://vps-10871.vps-ukraine.com.ua #


# Installation #

* git clone git@bitbucket.org:ilysenko/binary.git
* cd binary
* pip install -r requirements.txt

# Usage #

Start server:

python manage.py runserver
and go to link:

 http://127.0.0.1:8000/

# Requirements #

* Python 2.7.x
* Django 1.6+
* xmltodict 0.9+

# Limitations #

Maximum file size is 10 mb now