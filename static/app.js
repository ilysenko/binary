'use strict';

angular
    .module('app', ['angularFileUpload'])
    .controller('AppController', ['$scope', '$http', 'FileUploader', function ($scope, $http, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({
            url: constants.api_upload_file,
            formData: [
                { csrfmiddlewaretoken: $(csrf_token).val() }
            ]
        });

        $scope.autoConvert = true;

        $scope.processUploadedFiles = function () {
            _.each(constants.processed_files, function (i) {
                var new_item = {
                    file: {
                        name: i[1]
                    },
                    guid: i[0],
                    processed: true
                };
                uploader.queue.push(new_item);
            })
        }

        $scope.processUploadedFiles();

        $scope.deleteAll = function () {
            $http.get(constants.delete_url)
                .success(function (data, status) {
                    uploader.queue = [];
                }).error(function (data, status) {
                    $.jGrowl(data);
                });
        }

        $scope.downloadFile = function (url, name) {

            //Creating new link node.
            var link = document.createElement('a');
            link.href = url;

            if (link.download !== undefined) {
                //Set HTML5 download attribute. This will prevent file from opening if supported.
                link.download = name;
            }

            //Dispatching click event.
            var e = document.createEvent('MouseEvents');
            e.initEvent('click', true, true);
            var ua = window.navigator.userAgent;
            var old_ie = ua.indexOf('MSIE ');
            var new_ie = ua.indexOf('Trident/');

            if ((old_ie > -1) || (new_ie > -1)) {
                window.location = url;
            } else {
                link.dispatchEvent(e);
            }
            return true;
        }

        $scope.getExtensions = function () {
            return constants.valid_extensions;
        }

        $scope.getFileExtension = function (name) {
            var found = name.lastIndexOf('.') + 1;
            return (parseInt(found) > 0 ? name.substr(found) : "");
        }

        $scope.getFileName = function (name) {
            var found = name.lastIndexOf('.');
            return (parseInt(found) > 0 ? name.substr(0, found) : "");
        }

        $scope.bytesToSize = function (bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };

        $scope.processUpload = function (item, ext) {
            $http.get(constants.get_file_url, {
                params: {
                    original_name: item.guid + '/' + item.file.name,
                    extension: ext
                }
            }).success(function (data, status) {
                $scope.downloadFile(data.url, $scope.getFileName(item.file.name)
                    + '.' + ext.toLowerCase());
            }).error(function (data, status) {
                $.jGrowl(data);
            });
        }

        // FILTERS

        uploader.filters.push({
            name: 'checkExtenstion',
            fn: function (item) {
                var res = _.find(constants.valid_extensions, function (i) {
                    return i === $scope.getFileExtension(item.name).toUpperCase();
                });
                if (res === undefined) {
                    $.jGrowl('Not Valid File Extension');
                    return false;
                }
                ;
                var res = item.size <= constants.max_upload_size;
                if (!res) {
                    $.jGrowl('Maximum allowed file size is ' +
                        $scope.bytesToSize(constants.max_upload_size));
                    return false;
                }
                return true;
            }
        });

        uploader.filters.push({
                name: 'checkFileSize',
                fn: function (item) {
                    console.log(item.size, constants.max_upload_size);

                }
            }
        );

        // CALLBACKS

        uploader.onAfterAddingFile = function (fileItem) {
            if ($scope.autoConvert) uploader.uploadItem(fileItem);
        };

        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            fileItem.processed = true;
            fileItem.guid = response.result;
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            $.jGrowl(response);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
        };
    }]);