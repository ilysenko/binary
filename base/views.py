from django.views.generic import TemplateView
from converter.api import get_constants


class BaseTemplateView(TemplateView):
    template_name="base.html"

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['constants'] = get_constants()
        return context